using System.Collections.Generic;
using System.Threading.Tasks;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public interface IValidaItens
    {
        Item ValidarItems(IList<Item> Itens);
    }
}